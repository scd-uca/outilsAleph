#!/exlibris/aleph/a23_1/product/bin/perl

# Entrée : fichier Aleph séquentiel (nom codé en dur : numerosppn.seq)
# Sortie : fichier tabulé (nom codé en dur : tableaunotices.tsv) avec les colonnes suivantes : 
# numsys ; ppn ; ppnsfusion ; fmt ; anneepublication ; titre ; edition ; editeur ; collection ; format ; auteurspersonneprincipaux ; auteurscollectifsprincipaux


use strict;
use utf8;
use Unicode::Normalize;
use POSIX qw(strftime);

# Liste des paramètres de l'application
my $config;
my $champtodel = "";
my $champtokeep = "";
my $nottodel = "";
my $bin_path;
#my $alephe_scratch = $ENV{alephe_scratch};
#my $alephe_dev = $ENV{alephe_dev};
#my $aleph_proc = $ENV{aleph_proc};
#my $localhost = $ENV{ORA_HOST};

my $inputfile = "numerosppn.seq";
my $outputfile = "tableaunotices.tsv";



my $numsysencours = "";
my $notice = "";
my $ppn = "";
my $date = "";
my $titre = "";
my $edition = "";
my $editeur = "";
my $format = "";
my @auteurpersonneprincipal = ();
my @auteurcolprincipal = ();
my $collection="";
my $fmt = "";
my @ppnsfusion = ();


# Renvoi le contenu d'un sous-champ
sub retourne_souschamp
{
        my ($valeur,$ss_chp) = @_;
        my $elements;
        my $results;

        foreach $elements (split('\$\$' , $valeur))
        {
                if (substr($elements,0,length($ss_chp)) =~ /$ss_chp/i)
                {
                        $results = substr($elements,length($ss_chp));
                        goto FIN;
                }
        }
FIN:
return $results;
}


sub retourne_souschamps
# ne traite pas la ponctuation ; supprime les souschamps absents
{
        my ($valeur,$ss_champs) = @_;
        my $elements = "";
        my $results = "";
        foreach $elements (split('\$\$' , $valeur))
        {
                if (substr($elements,0,1) =~ /[$ss_champs]/)
                {
                        $results = $results . "\$\$". $elements;
                }
        }
FIN:
return $results;
}



open(FILE_IN, "< $inputfile");
binmode FILE_IN, ":utf8";
open(FILE_OUT, "> $outputfile");
binmode FILE_OUT, ":utf8";

print FILE_OUT "numsys" . "\t" . "ppn" . "\t". "ppnsfusion". "\t"."fmt"."\t". "anneepublication" . "\t"."titre". "\t"."edition". "\t"."editeur". "\t"."collection"."\t"."format". "\t"."auteurspersonneprincipaux". "\t"."auteurscollectifsprincipaux"."\n";




while (<FILE_IN>)
{
	# Normalize + traitements chevrons
	$_ =~ s//<</g;
	$_ =~ s//>>/g;
	$_ =~ s/<//g;
	$_ =~ s/>//g;
	my $ligne = Unicode::Normalize::compose($_);
	chomp($ligne);

# structure du fichier
#000000010 FMT   L BK
#000000010 LDR   L -----cam2-2200529---450-
#000000010 001   L PPN032055803
#000000010 100   L $$a19950111d1919----k--y0frey50------ba
#000000010 2001  L $$a<<La >>Grande-Bretagne au travail$$bTexte imprimé$$fJ.-F. Herbert...$$fGeorge Mathieu...
#000000010 205   L $$a2e édition
#000000010 210   L $$aParis$$cRoger$$d1919
#000000010 215   L $$a1 vol. (292 p. - 23 p. de pl.)$$cill.$$d21 cm

# numéro sys
	my $numsys = substr($ligne,0,9); if ($numsysencours == "") {$numsysencours = $numsys};
#champ
	my $champ = substr($ligne,10,3);
#indicateurs
	my $ind = substr($ligne,13,2);
#données
	my $data = substr($ligne,18);
	
			SWITCH: for ($champ)
		{
			/001/&& do  { $ppn = $data;};
			/FMT/&& do  { $fmt = $data;};
			/035/&& do  { $data =~ /\$\$a(.*)\$\$9sudoc/ ;
			if ($1 ne "") {
				
				push (@ppnsfusion, $1);
			}
			};
			/100/&& do  { $date = substr($data,12,4);};
			/200/&& do  { $titre = retourne_souschamps($data,"aeih");
			$titre =~ s/\$\$a// ;
			$titre =~ s/\$\$a/ ; / ;
			$titre =~ s/\$\$e/ : /g ;
			$titre =~ s/\$\$h/. / ;
			$titre =~ s/\$\$i/, / ;


};
			/205/&& do  { $edition =  retourne_souschamp($data,"a");};
			/210/&& do  { $editeur = retourne_souschamp($data,"c");};
			/215/&& do  { $format = retourne_souschamp($data,"a");};
			/225/&& do  { $collection = retourne_souschamp($data,"a");};
			/70[01]/&& do  { $data = retourne_souschamps($data,"ab");
			$data =~ s/\$\$a//g ;
			$data =~ s/\$\$b/, /g ;
			if ($data ne "") {
				push (@auteurpersonneprincipal, $data);
			}
			};
			/71[01]/&& do  { $data = retourne_souschamp($data,"a");
			if ($data ne "") {
				push (@auteurcolprincipal, $data);
			}};
		}

# passage à la notice suivante

	if ($numsys != $numsysencours)
	{
	 # print $numsysencours . "\t" . $ppn . "\t". join ("|",@ppnsfusion). "\t".$fmt."\t". $date . "\t".$titre. "\t".$edition. "\t".$editeur. "\t".$collection."\t".$format. "\t". join (" ; ",@auteurpersonneprincipal). "\t".join (" ; ",@auteurcolprincipal)."\n";
		print FILE_OUT $numsysencours . "\t" . $ppn . "\t". join ("|",@ppnsfusion). "\t".$fmt."\t". $date . "\t".$titre. "\t".$edition. "\t".$editeur. "\t".$collection."\t".$format. "\t". join (" ; ",@auteurpersonneprincipal). "\t".join (" ; ",@auteurcolprincipal)."\n";
		$ppn = "";
		$date = "";
		$titre = "";
		$edition = "";
		$editeur = "";
		$format = "";
		@auteurpersonneprincipal = ();
		@auteurcolprincipal = ();
		$collection = "";
		$fmt = "";
		@ppnsfusion = ();
		$numsysencours = $numsys
	}
	
}

close(FILE_IN);
close(FILE_OUT);
