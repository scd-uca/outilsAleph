#!/exlibris/aleph/a23_1/product/bin/perl

# Entrée : fichier tabulé (nom codé en dur : tableaunotices.tsv) avec les colonnes suivantes : 
# numsys ; ppn ; ppnsfusion ; fmt ; anneepublication ; titre ; edition ; editeur ; collection ; format ; auteurspersonneprincipaux ; auteurscollectifsprincipaux
# Sortie : fichier tabulé contenant les doublons de PPN (nom codé en dur : outputdoublonsppn.tsv) :
# ppn ; numsys (séparés par des |)


use strict;
use utf8;
use Unicode::Normalize;
use POSIX qw(strftime);

my $inputfile = "tableaunotices.tsv";
my $outputfile = "tableaunoticesdoublonsppn.tsv";




open(FILE_IN, "< $inputfile");
binmode FILE_IN, ":utf8";
open(FILE_OUT, "> $outputfile");
binmode FILE_OUT, ":utf8";

print FILE_OUT "ppn" ."\t" ."numsys"."\n";


my %ppn_numsys;
my %ppn_numsys_doublons;

while (<FILE_IN>)
{
	chomp $_;
	my @ligne = split(/\t/, $_);
	if (! $ppn_numsys{$ligne[1]}) 
		{
			$ppn_numsys {$ligne[1]} = $ligne[0];
		}
	else 
	{
		$ppn_numsys {$ligne[1]} = $ppn_numsys {$ligne[1]} . "|". $ligne[0];
		$ppn_numsys_doublons {$ligne[1]} = $ppn_numsys {$ligne[1]};
		}
	
}
close(FILE_IN);

while ( (my $k,my $v) = each %ppn_numsys_doublons ) {
    print FILE_OUT $k."\t".$v."\n";
}

close(FILE_OUT);
