#!/bin/csh -f

# Lancement : 
# csh -f /home/scd/remplacePPN.csh mode num oldppn newppn fichiersortie

# Script a deposer dans /home/scd
# Fichier d'entree dans $alephe_scratch
# Fichier de logs dans $alephe_scratch
# Fichier de sortie dans $uns01_dev/uns01/print

# SCRIPT APPELANT le service manage_21 pour modifier la zone 001 d'une notice. Le PPN existant est exige pour proceder a un contrôle eviter les erreurs (recours au service print_03)

# Syntaxe : 
# remplacePPN.csh mode num oldppn newppn fichiersortie
# mode : run (execute) ou dry-run (simule)
# num : numero systeme de la notice (9 chiffres, y compris 0 initiaux)
# oldppn : contenu de la zone 001 a remplacer (PPN, numero Electre etc)"
# newppn : PPN a integrer en zone 001 (doit debuter par le prefixe PPN)
# fichiersortie : nom du fichier de logs cree dans le dossier /exlibris/aleph/u23_1/uns01/print/ (minuscules, sans extension, sans espaces)
# Fichier de log : 1 ligne : 
# si l'operation s'est bien passee :
# OK,$num,$oldppn,$newppn
# sinon (mauvais PPN etc) : 
# KO,$num,$oldppn,$newppn


# Variables d'environnement utiles (deja presentes sur le serveur, pas la peine de les creer)
# uns01_dev=/exlibris/aleph/u23_1
# uns51_dev=/exlibris/aleph/u23_1
# data_print=$uns01_dev/uns01/print
# data_scratch=/exlibris/aleph/u23_1/uns01/scratch
# alephe_root=/exlibris/aleph/u23_1/alephe
# alephm_root=/exlibris/aleph/a23_1/alephm
# alephm_proc=/exlibris/aleph/a23_1/alephm/proc
# aleph_root=/exlibris/aleph/a23_1/aleph
# set alephe_scratch=/exlibris/aleph/u23_1/alephe/scratch


# Initialise l'environnement et les variables
# Configure l'environnement temporaire (pas forcement necessaire mais la plupart des scripts le font)
source $aleph_proc/def_local_env
# source $aleph_proc/unset_lib_env
source $uns01_dev/uns01/prof_library

set dossier_scratch = "$alephe_scratch"
set dossier_script = "/home/scd"
set dossier_sortieUNS01 = "$data_print"
set dossier_scratchUNS01 = "$data_scratch"

set fichierentree = "tempfichierentree"
set tempfichiersortie = "tempfichiersortie"
set fichierlogsbruts = "fichierlogsbruts"
set base = "UNS01"

if ( -f "$fichierentree" ) then
  echo "ERREUR : le fichier temporaire $fichierentree existe deja "
  goto fin
endif

if ( -f "$fichierlogsbruts" ) then
  echo "ERREUR : le fichier temporaire $fichierlogsbruts existe deja "
  goto fin
endif

# Recupere les parametres et verifie

if ($#argv != 5) then
  echo "ERREUR : Syntaxe attendue :"
  echo "remplacePPN.csh mode num oldppn newppn fichiersortie"
  echo "mode : run (execute) ou dry-run (simule)"
  echo "num : numero systeme de la notice (9 chiffres, y compris 0 initiaux)"
  echo "oldppn : contenu de la zone 001 a remplacer (PPN, numero Electre etc)"
  echo "newppn : PPN a integrer en zone 001 (doit debuter par le prefixe PPN)"
  echo "fichiersortie : nom du fichier de logs cree dans le dossier $data_print (minuscules, sans extension, sans espaces)"
  goto fin
endif

set mode = $1
set num = $2
set oldppn = $3
set newppn = $4
set fichiersortie = $5

if (("$mode" != "run") && ("$mode" != "dry-run")) then
  echo "ERREUR : mode doit contenir run ou dry-run"
  goto fin
endif

echo "...Verification des parametres : OK"

if ("$mode" == "run" ) then 
  echo "EXECUTION EN MODE RUN : LA NOTICE SERA MODIFEE"
else
  echo "EXECUTION EN MODE DRY-RUN : LA NOTICE NE SERA PAS MODIFEE"
endif

# CREATION D'UN FICHIER TEMPORAIRE D'ENTREE
echo $num$base > $dossier_scratch/$fichierentree

echo "...Creation d'un fichier temporaire d'entree : OK"

# VERIFICATION DE LA NOTICE : SERVICE PRINT_03

set p_active_library = "UNS01"
set p_file_in = "$fichierentree"
set p_buf_code_1 = "001"
set p_buf_code_2 = ""
set p_buf_code_3 = ""
set p_buf_code_4 = ""
set p_buf_code_5 = ""
set p_buf_code_6 = ""
set p_buf_code_7 = ""
set p_buf_code_8 = ""
set p_file_out = "$tempfichiersortie"
set p_file_format = "A" #sequentiel Aleph
set p_fix_routine = ""
set p_expand_routine = ""
set p_char_conv = ""
set p_export_del_records = "N"

set paramservice = "$p_active_library,$p_file_in,$p_buf_code_1,$p_buf_code_2,$p_buf_code_3,$p_buf_code_4,$p_buf_code_5,$p_buf_code_6,$p_buf_code_7,$p_buf_code_8,$p_file_out,$p_file_format,$p_fix_routine,$p_expand_routine,$p_char_conv,$p_export_del_records"

csh -f  $aleph_proc/p_print_03 "$paramservice"  >& /dev/null

set lignes_sortie = `wc -l $dossier_scratchUNS01/$tempfichiersortie | cut -f1 -d' '`
if ( "$lignes_sortie" == "0" ) then
  echo "ERREUR : la notice $num est introuvable dans Aleph"
  echo "Erreur (Notice introuvable),$num,$oldppn,$newppn" > $dossier_sortieUNS01/$fichiersortie
  echo "...Creation d'un fichier de logs : OK"
  goto affichelogs
endif

# Attendu
#000143365 001   L PPN005481082
set ppn_notice = `head -n1 "$dossier_scratchUNS01/$tempfichiersortie" | cut -c19-`
rm "$dossier_scratchUNS01/$tempfichiersortie"
if ( "$ppn_notice" != "$oldppn" ) then 
  echo "ERREUR : la zone 001 de la notice contient $ppn_notice au lieu de $oldppn"
  echo "Erreur (PPN introuvable dans la notice),$num,$oldppn,$newppn" > $dossier_sortieUNS01/$fichiersortie
  echo "...Creation d'un fichier de logs : OK"
  goto affichelogs
endif

echo "...Verification du PPN present dans la notice $num (service print_03) : OK"

# PARAMETRES DU SERVICE MANAGE_21

# SYNTAXE DE MANAGE 21
# MODIFICATION : remplacer PPN157775720 par PPNBIDON dans la 001
# UNS01,fichierentree,fichiersortie,Y,,001,#,#,,N,,,,,,,PPN157775720,BIDON,,,,,,,,N,,msaby, 
# SUPPRESSION de la 001 
# UNS01,fichierentree,fichiersortie,Y,,001,#,#,,Y,,,,,,,,,,,,,,,,N,,msaby, 
# AJOUTER une 001 avec PPNBIDON
# UNS01,fichierentree,fichiersortie,Y,,,,,,N,,,,,,,,,001,,,PPNBIDON,,,,N,,msaby, 

set p_active_library = "UNS01"
set p_input_file = "$fichierentree"
set p_output_file = "$fichierlogsbruts"
if ("$mode" == "run") then
  set p_update = "Y"
else 
  set p_update = "N"
endif
set p_param_file = ""
set p_tag = "001"
set p_first_ind = "#"
set p_second_ind = "#"
set p_match_text = ""
set p_delete_tag = "N"
set p_delete_sub_field_1 = ""
set p_delete_sub_field_2 = ""
set p_delete_sub_field_3 = ""
set p_delete_sub_field_4 = ""
set p_delete_sub_field_5 = ""
set p_new_data = ""
set p_string_replace = "$oldppn"
set p_string_by = "$newppn"
set p_new_tag = ""
set p_new_first_ind = ""
set p_new_second_ind = ""
set p_new_tag_data = ""
set p_change_tag = ""
set p_change_first_ind = ""
set p_change_second_ind = ""
set p_sort_tag = "N"
set p_sort_order = ""
# je laisse "cavalie" dans le doute (utilise dans d'autres scripts, il y ...)
set p_user_name = "cavalie"
set p_cataloger_in = "SCD"
set p_cataloger_level_x = "00"

set paramservice  = "$p_active_library,$p_input_file,$p_output_file,$p_update,$p_param_file,$p_tag,$p_first_ind,$p_second_ind,$p_match_text,$p_delete_tag,$p_delete_sub_field_1,$p_delete_sub_field_2,$p_delete_sub_field_3,$p_delete_sub_field_4,$p_delete_sub_field_5,$p_new_data,$p_string_replace,$p_string_by,$p_new_tag,$p_new_first_ind,$p_new_second_ind,$p_new_tag_data,$p_change_tag,$p_change_first_ind,$p_change_second_ind,$p_sort_tag,$p_sort_order,$p_user_name,$p_cataloger_in,$p_cataloger_level_x" 


#set paramservice = "UNS01,$fichierentree,$fichierlogsbruts,$modifiebase,,001,#,#,,N,,,,,,,$oldppn,$newppn,,,,,,,,N,,cavalie,SCD,00,"
# A tester :
#set paramservice = "UNS01,$fichierentree,$fichierlogsbruts,$modifiebase,,001,#,#,$oldppn,N,,,,,,,$oldppn,$newppn,,,,,,,,N,,cavalie,SCD,00,"

# LANCER LE SERVICE

csh -f  $aleph_proc/p_manage_21 "$paramservice"  >& /dev/null

echo "...Lancement du service manage_21 en mode $mode : OK"

# ANALYSER LE FICHIER DE LOGS BRUTS ET CREER UN FICHIER DE LOG PROPRES

@ nblignes = `grep -c new-text "$dossier_sortieUNS01/$fichierlogsbruts"`
if ("$nblignes" == 1) then 
  echo "OK,$num,$oldppn,$newppn" > $dossier_sortieUNS01/$fichiersortie
else
  echo "Erreur,$num,$oldppn,$newppn" > $dossier_sortieUNS01/$fichiersortie
endif

echo "...Creation d'un fichier de logs : OK"

affichelogs:
echo "Contenu du fichier de logs cree  ($dossier_sortieUNS01/$fichiersortie) : "
cat "$dossier_sortieUNS01/$fichiersortie"

fin:
# FIN
echo "...Fin du script et nettoyage : OK"
if ( -f $dossier_scratch/$fichierentree ) then 
  rm $dossier_scratch/$fichierentree
endif
if ( -f $dossier_sortieUNS01/$fichierlogsbruts  ) then 
  rm $dossier_sortieUNS01/$fichierlogsbruts 
endif
# supprime environnement temporaire
rm_f_symbol
