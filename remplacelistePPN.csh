#!/bin/csh -f


# Script à déposer dans $alephm_proc
# Fichier d'entrée dans $alephe_scratch
# Fichier de logs dans $alephe_scratch
# Fichier de sortie dans $uns01_dev/uns01/print

# Syntaxe : 
# remplacelistePPN.csh mode fichierentree fichiersortie
# mode : run (execute) ou dry-run (simule)
# fichierentree : nom du fichier d'entrée déposé dans le dossier $alephe_scratch
# fichiersortie : nom du fichier de logs créé dans le dossier /exlibris/aleph/u23_1/uns01/print/ (minuscules, sans extension, sans espaces)
# structure du fichier d'entrée: 3 informations par ligne (num de notice Aleph, ancien PPN, nouveau PPN), séparées par ; 
# num1;oldppn1;newppn1
# num2;oldppn2;newppn2
# num3;oldppn3;newppn3

# Configure l'environnement (pas forcément nécessaire mais la plupart des scripts le font)

source $aleph_proc/def_local_env
source $aleph_proc/unset_lib_env
source $uns01_dev/uns01/prof_library

# Variables d'environnement utiles
# uns01_dev=/exlibris/aleph/u20_1
# uns51_dev=/exlibris/aleph/u20_1
# data_print=$uns01_dev/uns01/print
# alephe_root=/exlibris/aleph/u20_1/alephe
# alephm_root=/exlibris/aleph/a20_1/alephm
# alephm_proc=/exlibris/aleph/a20_1/alephm/proc
# aleph_root=/exlibris/aleph/a20_1/aleph
# alephe_scratch=/exlibris/aleph/u20_1/alephe/scratch

set dossier_scratch = "$alephe_scratch"
set dossier_script = "$alephm_proc"
set dossier_sortie = "$data_print"
set fichiersortietemp = "fichiersortietemp"
set fichierafftemp = "fichierafftemp"

if ( -f "$fichiersortietemp" ) then
  echo "ERREUR : le fichier temporaire existe déjà "
  exit 1
endif

# Récupère les paramètres et vérifie

if ($#argv != 3) then
  echo "ERREUR : Syntaxe attendue :"
  echo "remplacelistePPN.csh mode fichierentree fichiersortie"
  echo "mode : run (execute) ou dry-run (simule)"
  echo "fichierentree : nom du fichier d'entrée déposé dans le dossier $alephe_scratch"
  echo "fichiersortie : nom du fichier de logs créé dans le dossier $data_print (minuscules, sans extension, sans espaces)"
  exit 1
endif

set mode = $1
set fichierentree = $2
set fichiersortie = $3



if ("$fichiersortietemp" == "$fichiersortie") then
  echo "ERREUR : changer le nom du fichier de sortie (collision avec fichier temporaire)"
  exit 1
endif

if ( -f "$dossier_scratch/$fichiersortietemp" ) then
  echo "ERREUR : fichier temporaire $fichiersortietemp déjà existant)"
  exit 1
endif

if ( -f "$dossier_sortie/$fichiersortie" ) then
  echo "ERREUR : changer le nom du fichier de sortie (fichier déjà existant)"
  exit 1
endif

if (("$mode" != "run") && ("$mode" != "dry-run")) then
  echo "ERREUR : "
  echo "mode doit contenir run ou dry-run"
  exit 1
endif


# Vérifie si le fichier entrée existe

if ( -f "$dossier_scratch/$fichierentree" ) then
    echo "Le fichier $fichierentree existe dans $dossier_scratch"
else
    echo "ERREUR : Le fichier $fichierentree n'existe pas dans $dossier_scratch"
    exit 1
endif

# Vérifie la syntaxe du fichier d'entrée

if (`cat "$dossier_scratch/$fichierentree" | tr -Cd ";\n" | uniq` == ";;" ) then
   echo "Le fichier $fichierentree a une structure correcte"
else
   echo "ERREUR : Le fichier $fichierentree a une structure incorrecte"
   exit 1
endif

# lit le fichier d'entrée
@ i = 1
foreach ligne ( `cat "$dossier_scratch/$fichierentree"` )
    set num = `echo "$ligne" | cut -d";" -f1`
    set oldppn = `echo "$ligne" | cut -d";" -f2`
    set newppn = `echo "$ligne" | cut -d";" -f3`
    echo "ligne $i : notice $num : $oldppn -> $newppn" >& $dossier_sortie/$fichierafftemp 
    csh -f  $dossier_script/remplacePPN.csh "$mode" "$num" "$oldppn" "$newppn" "$fichiersortietemp" >>& $dossier_sortie/$fichierafftemp 
    cat $dossier_sortie/$fichiersortietemp >> $dossier_sortie/$fichiersortietemp 
    @ i += 1
end

echo "Fin du script remplacelistePPN en mode $mode"  >>& $dossier_sortie/$fichierafftemp
echo "Logs d'execution : $dossier_sortie/$fichiersortie" >>& $dossier_sortie/$fichierafftemp
echo "Logs d'execution détaillés : $dossier_sortie/$fichiersortie" >>& $dossier_sortie/$fichierafftemp

cat $dossier_sortie/$fichierafftemp
rm $dossier_sortie/$fichiersortietemp
rm $dossier_sortie/$fichierafftemp

rm_f_symbol